import os

from dotenv import load_dotenv
from src.farm_generic import FarmLEDGeneric
from src.tuya_socket import TuyaSocket

load_dotenv()


farm_1 = FarmLEDGeneric(
            id=1,
            alias="farm-183-3",
            layer_1_tuya_socket=TuyaSocket(
                id=1, 
                layer_id=1, 
                tuya_id=os.getenv('FARM_183_3_LAYER_1_TUYA_DEVICE_ID'), 
                ip_address=os.getenv('FARM_183_3_LAYER_1_IP_ADDR'), 
                tuya_key=os.getenv('FARM_183_3_LAYER_1_TUYA_KEY_ID')),
            layer_2_tuya_socket=TuyaSocket(
                id=2, 
                layer_id=2, 
                tuya_id=os.getenv('FARM_183_3_LAYER_2_TUYA_DEVICE_ID'), 
                ip_address=os.getenv('FARM_183_3_LAYER_2_IP_ADDR'), 
                tuya_key=os.getenv('FARM_183_3_LAYER_2_TUYA_KEY_ID')),
            layer_3_tuya_socket=TuyaSocket(
                id=3, 
                layer_id=3, 
                tuya_id=os.getenv('FARM_183_3_LAYER_3_TUYA_DEVICE_ID'), 
                ip_address=os.getenv('FARM_183_3_LAYER_3_IP_ADDR'), 
                tuya_key=os.getenv('FARM_183_3_LAYER_3_TUYA_KEY_ID')),
            layer_4_tuya_socket=TuyaSocket(
                id=4, 
                layer_id=4, 
                tuya_id=os.getenv('FARM_183_3_LAYER_4_TUYA_DEVICE_ID'), 
                ip_address=os.getenv('FARM_183_3_LAYER_4_IP_ADDR'), 
                tuya_key=os.getenv('FARM_183_3_LAYER_4_TUYA_KEY_ID')),
            )
farm_2 = FarmLEDGeneric(
            id=2,
            alias="farm-183-2",
            layer_1_tuya_socket=TuyaSocket(
                id=1, 
                layer_id=1, 
                tuya_id=os.getenv('FARM_183_2_LAYER_1_TUYA_DEVICE_ID'), 
                ip_address=os.getenv('FARM_183_2_LAYER_1_IP_ADDR'), 
                tuya_key=os.getenv('FARM_183_2_LAYER_1_TUYA_KEY_ID')),
            layer_2_tuya_socket=TuyaSocket(
                id=2, 
                layer_id=2, 
                tuya_id=os.getenv('FARM_183_2_LAYER_2_TUYA_DEVICE_ID'), 
                ip_address=os.getenv('FARM_183_2_LAYER_2_IP_ADDR'), 
                tuya_key=os.getenv('FARM_183_2_LAYER_2_TUYA_KEY_ID')),
            layer_3_tuya_socket=TuyaSocket(
                id=3, 
                layer_id=3, 
                tuya_id=os.getenv('FARM_183_2_LAYER_3_TUYA_DEVICE_ID'), 
                ip_address=os.getenv('FARM_183_2_LAYER_3_IP_ADDR'), 
                tuya_key=os.getenv('FARM_183_2_LAYER_3_TUYA_KEY_ID')),
            layer_4_tuya_socket=TuyaSocket(
                id=4, 
                layer_id=4, 
                tuya_id=os.getenv('FARM_183_2_LAYER_4_TUYA_DEVICE_ID'), 
                ip_address=os.getenv('FARM_183_2_LAYER_4_IP_ADDR'), 
                tuya_key=os.getenv('FARM_183_2_LAYER_4_TUYA_KEY_ID')),
            )


def _switch_case_farm_id(argument):
    switcher = {
            1: farm_1,
            2: farm_2,
        }
    try:
        switcher[argument]
        return switcher.get(argument, False)
    except KeyError:
        print(f"Error farm_id argument: {argument}")
        return False

if __name__ == '__main__':
    r = _switch_case_farm_id(argument=1)