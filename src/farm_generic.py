try:
    from tuya_socket import TuyaSocket
except ImportError:
    from src.tuya_socket import TuyaSocket


class FarmLEDGeneric(object):
    def __init__(
            self,
            id: int,
            alias: str,
            layer_1_tuya_socket: TuyaSocket,
            layer_2_tuya_socket: TuyaSocket,
            layer_3_tuya_socket: TuyaSocket,
            layer_4_tuya_socket: TuyaSocket) -> None:
        self.id =id
        self.alias = alias
        self._TOTAL_LAYER = 4
        self.layer_1_tuya_socket = layer_1_tuya_socket
        self.layer_2_tuya_socket = layer_2_tuya_socket
        self.layer_3_tuya_socket = layer_3_tuya_socket
        self.layer_4_tuya_socket = layer_4_tuya_socket
    
    def __repr__(self,) -> str:
        return (f"FarmLEDGeneric({self.id}, {self.alias}, "
                    f"{self.layer_1_tuya_socket}, {self.layer_2_tuya_socket}, "
                    f"{self.layer_3_tuya_socket}, {self.layer_4_tuya_socket})")
    
    def _switch_case_layer_socket(self, argument: int):
        switcher = {
            1: self.layer_1_tuya_socket,
            2: self.layer_2_tuya_socket,
            3: self.layer_3_tuya_socket,
            4: self.layer_4_tuya_socket,}
        try:
            switcher[argument]
            return switcher.get(argument, False)
        except KeyError:
            print(f"FarmGeneric layer_id error: {argument}")
            return False
    
    def get_layer_socket_power_status(
            self, 
            layer_id: int = None,
            ) -> list:
        if layer_id is None:
            results = []
            for i in range(1, self._TOTAL_LAYER+1):
                layer_socket_id = self._switch_case_layer_socket(argument=i)
                results.append(layer_socket_id.get_power_status())
            return results
        layer_socket_id = self._switch_case_layer_socket(argument=layer_id)
        if not(layer_socket_id): return {}
        return [layer_socket_id.get_power_status()]
    
    def set_layer_socket_setpoint(
            self, 
            layer_id: int = None, 
            setpoint: bool = False,
            ) -> dict:
        # Configure all layers
        if layer_id is None:
            results = []
            for i in range(1, self._TOTAL_LAYER+1):
                layer_socket_id = self._switch_case_layer_socket(argument=i)
                results.append(layer_socket_id.turn_on() if setpoint 
                                else layer_socket_id.turn_off())
            return results
        # Configured fixed layer
        layer_socket_id = self._switch_case_layer_socket(argument=layer_id)
        if not(layer_socket_id): return {}
        return layer_socket_id.turn_on() if setpoint else layer_socket_id.turn_off()
