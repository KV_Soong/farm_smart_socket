import tinytuya


class TuyaSocket(object):
    def __init__(
            self, 
            id: int, 
            layer_id: int, 
            tuya_id: str, 
            ip_address: str, 
            tuya_key: str):
        self.id = id
        self.layer_id = layer_id
        self._ip_address = ip_address
        self._tuya_id = tuya_id
        self._tuya_key = tuya_key
        self.__library = tinytuya
        
        self._socket = self.__library.OutletDevice(
                        dev_id=self._tuya_id, 
                        address=self._ip_address,
                        local_key=self._tuya_key,)
        self._socket.set_version(version=3.3)
        self._socket.set_socketPersistent(persist=True)
        
        self.power_data = {
            'voltage:V': None,
            'current:A': None,
            'power:W': None
        }
    
    def __repr__(self) -> str:
        return (f"tuyaSocketProps({self.id}, {self.layer_id}, {self._ip_address}, "
                f"{self._tuya_id}, {self._tuya_key})")
    
    @property
    def socket_props(self,):
        return self._ip_address, self._tuya_id, self._tuya_key
    
    def _power_status(self,):
        self._socket.updatedps(index=[18, 19, 20])
        return self._socket.status()
    
    def turn_on(self,):
        # TODO: indicate return type
        return self._socket.turn_on()

    def turn_off(self,):
        # TODO: indicate return type
        return self._socket.turn_off()
    
    def get_power_status(self,) -> dict:
        data = None
        trial_counter = 0
        trial_counter_limit = 5
        while data is None and trial_counter < trial_counter_limit:
            data = self._power_status()
            trial_counter += 1
        if data is None:
            self.power_data['voltage:V'] = None
            self.power_data['current:A'] = None
            self.power_data['power:W'] = None
        else:
            self.power_data['voltage:V'] = data['dps']['20'] / 10 if '20' in data['dps'] else None
            self.power_data['current:A'] = data['dps']['18'] / 1000 if '18' in data['dps'] else None
            self.power_data['power:W'] = data['dps']['19'] / 1000 if '19' in data['dps'] else None
        return self.power_data

if __name__ == '__main__':
    socket_1 = TuyaSocket(id=1, layer_id=1, tuya_id="eb1ed6ebd8890625cad24h", ip_address="192.168.8.133", tuya_key="5937ffa2c3440f41")
    socket_1.get_power_status()
